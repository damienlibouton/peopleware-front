import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApplicantService } from '../applicant.service';
import { FormBuilder, FormControl, FormArray, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'pw-add-applicant',
  templateUrl: './add-applicant.component.html',
  styleUrls: ['./add-applicant.component.css']
})
export class AddApplicantComponent implements OnInit {
  fullNameCtrl: FormControl;
  contactCtrl: FormControl;
  emailCtrl: FormControl;
  workingTimeCtrl: FormControl;
  salaryCtrl: FormControl;

  applicantForm: FormGroup;

  workingTimes = [
    { name: 'FULL_TIME', abbrev: 'Full' },
    { name: 'PART_TIME', abbrev: 'Part' },
    { name: 'FULL_TIME_AND_PART_TIME', abbrev: 'Both' }
  ];


  constructor(private formBuilder: FormBuilder, private router: Router, private applicantService: ApplicantService) { }

  ngOnInit() {
    this.fullNameCtrl = this.formBuilder.control('', Validators.required);
    this.contactCtrl = this.formBuilder.control('', Validators.required);
    this.emailCtrl = this.formBuilder.control('', Validators.required);
    this.salaryCtrl = this.formBuilder.control('', Validators.required);
    this.workingTimeCtrl = this.formBuilder.control(this.workingTimes[0].name, Validators.required);

    this.applicantForm = this.formBuilder.group({
      fullName: this.fullNameCtrl,
      contact: this.contactCtrl,
      email: this.emailCtrl,
      salary: this.salaryCtrl,
      workingTime: this.workingTimeCtrl,
      degrees: this.formBuilder.array([]),
      skills: this.formBuilder.array([])
    });
  }

  get degreeForms() {
    return this.applicantForm.get('degrees') as FormArray;
  }

  get skillForms() {
    return this.applicantForm.get('skills') as FormArray;
  }

  addDegree() {
    const degree = this.formBuilder.group({
      name: this.formBuilder.control('', Validators.required)
    });

    this.degreeForms.push(degree);
  }

  removeDegreeItem(i: number) {
    this.degreeForms.removeAt(i);
  }


  addSkill() {
    const skill = this.formBuilder.group({
      name: this.formBuilder.control('', Validators.required),
      knowledgeLevel: this.formBuilder.control(1,
        [
          Validators.required,
          Validators.min(1),
          Validators.max(5)
        ])});
    
    this.skillForms.push(skill);
  }

  removeSkillItem(i: number): void {
    this.skillForms.removeAt(i);
  }

  addApplicant() {
    const applicant = {
      id: 0,
      knowledgeLevelOfRequiredSkills: 0,
      fullName: this.applicantForm.value.fullName,
      contactNumber: this.applicantForm.value.contact,
      monthlyMinSalary: this.applicantForm.value.salary,
      email: this.applicantForm.value.email,
      workingTime: this.applicantForm.value.workingTime.name,
      degrees: this.degreeForms.value,
      skills: this.skillForms.value
    };
    

    this.applicantService.add(applicant)
      .subscribe(
        () => this.router.navigate(['/applicants'])
      );
  }

}
