import { Component, OnInit } from '@angular/core';
import { JobService } from '../job.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { CompanyService } from '../company.service';
import { Company } from '../models/company.model';

@Component({
  selector: 'pw-add-job',
  templateUrl: './add-job.component.html',
  styleUrls: ['./add-job.component.css']
})
export class AddJobComponent implements OnInit {
  nameCtrl: FormControl;
  descriptionCtrl: FormControl;
  minYearlySalaryCtrl: FormControl;
  maxYearlySalaryCtrl: FormControl;
  workingTimeCtrl: FormControl;

  jobForm: FormGroup;

  workingTimes = [
    { name: 'FULL_TIME', abbrev: 'Full' },
    { name: 'PART_TIME', abbrev: 'Part' }
  ];

  company: Company;
  
  constructor(private formBuilder: FormBuilder, private router: Router, private jobService: JobService, private companyService: CompanyService, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('companyId');
    this.companyService.get(id).subscribe(company => this.company = company);

    this.nameCtrl = this.formBuilder.control('', Validators.required);
    this.descriptionCtrl = this.formBuilder.control('', Validators.required);
    this.minYearlySalaryCtrl = this.formBuilder.control('', Validators.required);
    this.maxYearlySalaryCtrl = this.formBuilder.control('', Validators.required);
    this.workingTimeCtrl = this.formBuilder.control(this.workingTimes[0].name, Validators.required);

    this.jobForm = this.formBuilder.group({
      name: this.nameCtrl,
      description: this.descriptionCtrl,
      minYearlySalary: this.minYearlySalaryCtrl,
      maxYearlySalary: this.maxYearlySalaryCtrl,
      workingTime: this.workingTimeCtrl,
      degrees: this.formBuilder.array([]),
      skills: this.formBuilder.array([])
    });
  }

  get degreeForms() {
    return this.jobForm.get('degrees') as FormArray;
  }

  get skillForms() {
    return this.jobForm.get('skills') as FormArray;
  }

  addDegree() {
    const degree = this.formBuilder.group({
      name: this.formBuilder.control('', Validators.required)
    });

    this.degreeForms.push(degree);
  }

  removeDegreeItem(i: number) {
    this.degreeForms.removeAt(i);
  }


  addSkill() {
    const skill = this.formBuilder.group({
      name: this.formBuilder.control('', Validators.required),
      knowledgeLevel: this.formBuilder.control(1,
        [
          Validators.required,
          Validators.min(1),
          Validators.max(5)
        ])});
    
    this.skillForms.push(skill);
  }

  removeSkillItem(i: number): void {
    this.skillForms.removeAt(i);
  }

  addJob() {
    const job = {
      id: 0,
      name: this.jobForm.value.name,
      description: this.jobForm.value.description,
      minYearlySalary: this.jobForm.value.minYearlySalary,
      maxYearlySalary: this.jobForm.value.maxYearlySalary,
      workingTime: this.jobForm.value.workingTime.name,
      degrees: this.degreeForms.value,
      skills: this.skillForms.value,
      companyId: this.company.id,
      companyName: this.company.name,
      companyContactNumber: this.company.contactNumber
    };
    

    this.jobService.add(job, this.company.id)
      .subscribe(
        () => this.router.navigate(['/companies'])
      );
  }

}
