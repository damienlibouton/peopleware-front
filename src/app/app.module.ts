import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { ApplicantsComponent } from './applicants/applicants.component';
import { CompaniesComponent } from './companies/companies.component';
import { JobsComponent } from './jobs/jobs.component';
import { MatchedJobsComponent } from './matched-jobs/matched-jobs.component';
import { MatchedApplicantsComponent } from './matched-applicants/matched-applicants.component';
import { AddJobComponent } from './add-job/add-job.component';
import { AddApplicantComponent } from './add-applicant/add-applicant.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ApplicantsComponent,
    CompaniesComponent,
    JobsComponent,
    MatchedJobsComponent,
    MatchedApplicantsComponent,
    AddJobComponent,
    AddApplicantComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
