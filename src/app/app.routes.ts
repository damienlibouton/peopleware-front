import { Routes } from '@angular/router';

import { ApplicantsComponent } from './applicants/applicants.component';
import { JobsComponent } from './jobs/jobs.component';
import { CompaniesComponent } from './companies/companies.component';
import { MatchedJobsComponent } from './matched-jobs/matched-jobs.component';
import { MatchedApplicantsComponent } from './matched-applicants/matched-applicants.component';
import { AddApplicantComponent } from './add-applicant/add-applicant.component';
import { AddJobComponent } from './add-job/add-job.component';

export const ROUTES: Routes = [
    { path: '', component: CompaniesComponent },
    { path: 'companies', component: CompaniesComponent },
    { path: 'jobs', component: JobsComponent },
    { path: 'jobs/:jobId', component: MatchedApplicantsComponent },
    { path: 'applicants', component: ApplicantsComponent },
    { path: 'applicants/:applicantId', component: MatchedJobsComponent },
    { path: 'applicant', component: AddApplicantComponent },
    { path: 'job/:companyId', component: AddJobComponent }
];
