import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../environments/environment';
import { Applicant } from './models/applicant.model';
import { Job } from './models/job.model';

@Injectable({
  providedIn: 'root'
})
export class ApplicantService {

  constructor(private http: HttpClient) { }

  list(): Observable<Array<Applicant>> {
    return this.http.get<Array<Applicant>>(`${environment.baseUrl}/api/applicant/`);
  }

  listJobsMatchingApplicant(applicantId: number): Observable<Array<Job>> {
    return this.http.get<Array<Job>>(`${environment.baseUrl}/api/applicant/${applicantId}/job/`);
  }

  add(applicant: Applicant): Observable<Applicant> {
    return this.http.post<Applicant>(`${environment.baseUrl}/api/applicant/`, applicant);
  }

  get(applicantId: number): Observable<Applicant> {
    return this.http.get<Applicant>(`${environment.baseUrl}/api/applicant/${applicantId}/`);
  }
}
