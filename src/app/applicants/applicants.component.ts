import { Component, OnInit } from '@angular/core';

import { Applicant } from '../models/applicant.model';
import { ApplicantService } from '../applicant.service';

@Component({
  selector: 'pw-applicants',
  templateUrl: './applicants.component.html',
  styleUrls: ['./applicants.component.css']
})
export class ApplicantsComponent implements OnInit {

  applicants: Array<Applicant> = [];

  constructor(private applicantService: ApplicantService) { }

  ngOnInit() {
    this.applicantService.list().subscribe(applicants => this.applicants = applicants);
  }

}
