import { Component, OnInit } from '@angular/core';

import { Company } from '../models/company.model';
import { CompanyService } from '../company.service';


@Component({
  selector: 'pw-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit {

  companies: Array<Company> = [];

  constructor(private companyService: CompanyService) { }

  ngOnInit() {
    this.companyService.list().subscribe(companies => this.companies = companies);
  }
}
