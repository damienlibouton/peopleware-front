import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../environments/environment';
import { Company } from './models/company.model';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private http: HttpClient) { }

  list(): Observable<Array<Company>> {
    return this.http.get<Array<Company>>(`${environment.baseUrl}/api/company/`);
  }

  get(jobId: number): Observable<Company> {
    return this.http.get<Company>(`${environment.baseUrl}/api/company/${jobId}/`);
  }
}
