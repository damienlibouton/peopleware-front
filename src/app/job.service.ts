import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../environments/environment';
import { Job } from './models/job.model';
import { Applicant } from './models/applicant.model';

@Injectable({
  providedIn: 'root'
})
export class JobService {

  constructor(private http: HttpClient) { }

  list(): Observable<Array<Job>> {
    return this.http.get<Array<Job>>(`${environment.baseUrl}/api/job/`);
  }

  listApplicantsMatchingJob(jobId: number): Observable<Array<Applicant>> {
    return this.http.get<Array<Applicant>>(`${environment.baseUrl}/api/job/${jobId}/applicant/`);
  }

  get(jobId: number): Observable<Job> {
    return this.http.get<Job>(`${environment.baseUrl}/api/job/${jobId}/`);
  }

  add(job: Job, companyId: number): Observable<Job> {
    return this.http.post<Job>(`${environment.baseUrl}/api/company/${companyId}/job/`, job);
  }
}