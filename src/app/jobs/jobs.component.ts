import { Component, OnInit } from '@angular/core';

import { Job } from '../models/job.model';
import { JobService } from '../job.service';

@Component({
  selector: 'pw-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css']
})
export class JobsComponent implements OnInit {
  
  jobs: Array<Job> = [];

  constructor(private jobService: JobService) { }

  ngOnInit() {
    this.jobService.list().subscribe(jobs => this.jobs = jobs);
  }

}
