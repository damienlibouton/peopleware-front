import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchedApplicantsComponent } from './matched-applicants.component';

describe('MatchedApplicantsComponent', () => {
  let component: MatchedApplicantsComponent;
  let fixture: ComponentFixture<MatchedApplicantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchedApplicantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchedApplicantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
