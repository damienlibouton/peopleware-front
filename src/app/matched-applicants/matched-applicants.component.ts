import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { JobService } from '../job.service';
import { Applicant } from '../models/applicant.model';
import { Job } from '../models/job.model';

@Component({
  selector: 'pw-matched-applicants',
  templateUrl: './matched-applicants.component.html',
  styleUrls: ['./matched-applicants.component.css']
})
export class MatchedApplicantsComponent implements OnInit {

  applicants: Array<Applicant> = [];
  job: Job;

  constructor(private jobService: JobService, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('jobId');
    this.jobService.listApplicantsMatchingJob(id).subscribe(applicants => this.applicants = applicants);
    this.jobService.get(id).subscribe(job => this.job = job);
  }

}
