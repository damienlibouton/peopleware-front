import { Component, OnInit } from '@angular/core';
import { Job } from '../models/job.model';
import { ApplicantService } from '../applicant.service';
import { ActivatedRoute } from '@angular/router';
import { Applicant } from '../models/applicant.model';

@Component({
  selector: 'pw-matched-jobs',
  templateUrl: './matched-jobs.component.html',
  styleUrls: ['./matched-jobs.component.css']
})
export class MatchedJobsComponent implements OnInit {

  jobs: Array<Job> = [];
  applicant: Applicant;
  requiredKnowledge: number;

  constructor(private applicantService: ApplicantService, private route: ActivatedRoute) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('applicantId');
    this.applicantService.listJobsMatchingApplicant(id).subscribe(jobs => this.jobs = jobs);
    this.applicantService.get(id).subscribe(applicant => this.applicant = applicant);
  }

  sumRequiredKnowledge(job: Job): string {
    let sum = 0;
    job.skills.forEach(skill => sum += skill.knowledgeLevel);
    return ""+sum;
  }

}
