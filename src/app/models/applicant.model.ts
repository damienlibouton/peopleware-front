import { Degree } from './degree.model';
import { Skill } from './skill.model';


export interface Applicant {
    id: number;
    fullName: string;
    contactNumber: string;
    email: string;
    monthlyMinSalary: number;
    degrees: Degree[];
    skills: Skill[];
    workingTime: string;
    knowledgeLevelOfRequiredSkills: number;
}


