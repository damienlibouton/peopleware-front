import { Job } from './job.model';

export interface Company {
    id: number;
    name: string;
    contactNumber: string;
    jobs: Job[];
}

