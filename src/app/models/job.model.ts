import { Degree } from './degree.model';
import { Skill } from './skill.model';

export interface Job {
    id: number;
    name: string;
    description: string;
    minYearlySalary: number;
    maxYearlySalary: number;
    workingTime: string;
    degrees: Degree[];
    skills: Skill[];
    companyId: number;
    companyName: string;
    companyContactNumber: string;
}



