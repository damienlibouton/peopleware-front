export interface Skill {
    name: string;
    knowledgeLevel: number;
}